#include "../test/segment_ray_intersection_test.h"
#include "../profiler/profile.h"

int main() {
  { LOG_DURATION("TestAll")
    TestAll();
  }

  return 0;
}
