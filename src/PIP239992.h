#ifndef RGC_1_PIP239992_H
#define RGC_1_PIP239992_H

#include <CGAL/Simple_cartesian.h>
#include <CGAL/intersections.h>

namespace N239992 {
  typedef CGAL::Simple_cartesian<double> kernel_t;
  typedef kernel_t::Point_2 point_t;
  typedef kernel_t::Line_2 line_t;
  typedef kernel_t::Ray_2 ray_t;

  bool SegmentRayIntersected(const point_t& p, const point_t& q, const point_t& r) {
    if (r.y() > std::max(p.y(), q.y()) ||
        r.y() < std::min(p.y(), q.y())) {
      return false;
    }

    const line_t line(p, q);
    const ray_t ray(r, point_t{r.x() + 1.0, r.y()});
    return static_cast<bool>(CGAL::intersection(ray, line));
  }
}

#endif //RGC_1_PIP239992_H
