#ifndef RGC_1_SEGMENT_RAY_INTERSECTION_TEST_H
#define RGC_1_SEGMENT_RAY_INTERSECTION_TEST_H

#include "test_runner.h"
#include "../src/PIP239992.h"

void SimpleTest() {
  using namespace N239992;
  point_t p{0.0, -1.0};
  point_t q{0.0, 1.0};
  point_t r{0.0, 0.0};

  Assert(SegmentRayIntersected(p, q, r), "SimpleTest");
  Assert(SegmentRayIntersected(q, p, r), "SimpleTest2");
  Assert(SegmentRayIntersected(p, q, p), "SimpleTest3");
  Assert(SegmentRayIntersected(p, q, q), "SimpleTest4");

  Assert(!SegmentRayIntersected(q, q, r), "Point-RayTest");
  Assert(!SegmentRayIntersected(p, p, r), "Point-RayTest2");

  Assert(SegmentRayIntersected(p, p, p), "Point-PointTest");
  Assert(SegmentRayIntersected(q, q, q), "Point-PointTest2");
  Assert(SegmentRayIntersected(r, r, r), "Point-PointTest3");
}

void TestAll() {
  TestRunner tr;
  tr.RunTest(SimpleTest, "SimpleTest");
}

#endif //RGC_1_SEGMENT_RAY_INTERSECTION_TEST_H
